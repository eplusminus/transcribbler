
Transcribbler is a Mac OS X text editor with extensions for audio/video transcription.  It doesn't do
voice recognition, it just helps you type as quickly as possible using customizable keyboard controls and
word/phrase abbreviations.  It uses basic Cocoa API components for text editing, and QuickTime for audio
and video playback.

This was inspired by a wide variety of similar tools that I've used as a transcriptionist since the 1990s.
They all do more or less the same things, but none of them worked quite the way I wanted them to.

So far it's only been tested on OS 10.7 (Lion), but _should_ also work with 10.6 (Snow Leopard) and 10.8
(Mountain Lion).  I know a lot of transcribers have older systems, so I'm hoping to add 10.5 (Leopard)
compatibility, but that's a little tricky to do.

[Source code](https://bitbucket.org/eplusminus/transcribbler/src) •
[Documentation](https://bitbucket.org/eplusminus/transcribbler/wiki) •
[Bug/feature tracking](https://bitbucket.org/eplusminus/transcribbler/issues)